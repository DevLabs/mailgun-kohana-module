<?php defined('SYSPATH') OR die('No direct access allowed.');
return array(
	/**
	 * README:
	 * For the DevLabs API key and default Domain look in кирийки, or ask nicely veselin@devlabs.bg
	 */

	/**
	 * The api key is from your MailGun registration, starts with key- and the auto generated key
	 */
	'key-api' => '',
	
	/**
	 * In order to use a specific domain you must have it registered and verified in MailGun.
	 * To learn about domains go to: http://documentation.mailgun.com/quickstart-sending.html#how-to-verify-your-domain
	 */
	'domain' => 'example.com',

	/**
	 * A default From email
	 */
	'from' => 'noreply@example.com',

	/**
	 * A default From name
	 */
	'fromName' => 'Automatic Email',
);