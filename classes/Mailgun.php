<?php defined('SYSPATH') OR die('No direct access allowed.');

/**
 * A wrapper class for MailGun API found at: https://github.com/mailgun/mailgun-php
 * Use Mailgun::instance() to get an instance of the Mailgun API
 * Or use Mailgun::send helper to just send a message
 * Please set the config file accordingly with api-key and domain
 *
 * @author     Veselin Bratanov <veselin@devlabs.bg>
 * @copyright  2014-present - DevLabs.bg
 */
class Mailgun {

	/**
	 * $domain The domain is set automatically when you call instance() from config
	 * You can change it if you want.
	 * Register your domain in Mailgun before sending from it, or use the sandbox domain for development
	 * 
	 * @var string
	 */
	public static $domain;

	/**
	 * $instance The MailGun instance
	 * 
	 * @var object MailGun object
	 */
	protected static $instance;

	/**
	 * Returns the current or creates a new a MailGun instance.
	 * Also loads the domain from config
	 * 
	 * @return  object  MailGun object
	 */
	public static function instance()
	{
		if( ! isset(Mailgun::$instance))
		{
			//Autoload the required files for mailgun
			require_once Kohana::find_file('vendor', 'autoload_mailgun', 'php');

			//Initialize the instance
			Mailgun::$instance = new Mailgun\Mailgun(Kohana::$config->load('mailgun')->get('key-api'));
			//Set the default domain from config
			Mailgun::$domain = Kohana::$config->load('mailgun')->get('domain');
		}

		//Return the instance to MailGun APi
		return Mailgun::$instance;
	}

	/**
	 * A helper function - wrapper for MailGun's sendMessage function.
	 * If you want to use more advanced stuff you should use Mailgun::instance()->sendMessage and the other appropriate 
	 * methods you get in the MailGun PHP API: https://github.com/mailgun/mailgun-php
	 * 
	 * @param  mixed  	$to      Recipient in the format 'Cool Name <email@mail.com>' or 'email@mail.com', or an array of those containing multiple recipients
	 * @param  mixed  	$from    The sender in format 'Cool Name <email@mail.com>' or 'email@mail.com'. 
	 *                          It's recommended to use an email on the same domain as the domain in config but anything can be used
	 * @param  string   $subject 	The subject of the email
	 * @param  string   $message The content of the email
	 * @param  boolean  $html    Wether the Email is set as HTML or not (defaults to false for compatibility)
	 * @param  string   $webhook_to_url    The URL that will receive webhooks for this request 
	 *                                     (This is valid only if the webhook in MailGun for this domain is set to /mailgunwebhook/webhook.php
	 *                                     It's a global webhook which security validates and redirects requests to your URL,
	 *                                     If you set up a different webhook you'll receive your mails there)
	 *                                     Leave empty if you don't want to receive webhooks
	 *                                     For more info on webhooks go to http://documentation.mailgun.com/user_manual.html#webhooks
	 * @param  array    $webhook_custom_parameters 	An array of data to be send with the email that will be received with the webhooks, 
	 *                                              use it to send any tokens/ids that will uniquely identify this email if you want to process it.
	 * @return array 	The result from MailGun, usually contains status code and other useful information
	 */
	public static function send($to, $subject, $message, $html = FALSE, $from = '', $webhook_to_url = '', $webhook_custom_paramters = array())
	{
		//Get the mailgun instance
		$mailgun = Mailgun::instance();

		//Get the domain name
		$domain = Mailgun::$domain;

		//If we don't set a sender use noreply@domain.com
		if(! isset($from) || empty($from))
		{
			$from = 'noreply@'.$domain;
		}

		$request = array(
					'from' 		=> $from,
					'to'      	=> $to,
					'subject' 	=> $subject, 
					$html ? 'html' : 'text' => $message,
					'h:x-mailgun-native-send' => true, //mailgun native send prevents sender callback verification that causes issues 
				);

		//Check for a wanted webhook and execute it
		if($webhook_to_url != '')
		{
			$request['v:DEVLABS-WEBHOOK-URL'] = $webhook_to_url;

			//Add the custom parameters to the request
			foreach ($webhook_custom_paramters as $key => $param)
			{
				$request['v:' . $key] = $param;
			}
		}

		//Send the email trough the MailGun API
		$result = $mailgun->sendMessage(
			$domain, 
			$request
		);

		//Return the result from MailGun
		return $result;
	}

}